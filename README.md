- create a machine on AWS and install docker using `sudo yum install docker`
- Add user to the docker group `sudo usermod -aG docker $USER`
- Install gitlab runner ` curl -LJO "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/rpm/gitlab-runner_amd64.rpm"`
- Unpack it using `sudo rpm -i gitlab-runner_amd64.rpm`

- Go to https://gitlab.com/kr71/gitlab-runner-deployment/-/settings/ci_cd and expand the runner section and then create a new project runner. 
- You will get a command with a token ` sudo gitlab-runner register  --url https://gitlab.com  --token glrt-<token>`
- Run in on your system. 
- Once it's done, use docker executor and choose `cgr.dev/chainguard/wolfi-base:latest` image (this works best for me, please choose yours accordingly).
- disable shared runners if you only want to use yours. You should see a green icon 

![Alt text](./images/gitlab-runner.png)
- Add a demo pipeline, already covered by gitlab when you go ahead and create a pipeline. 
- This should be it. You should see your pipeline running. 